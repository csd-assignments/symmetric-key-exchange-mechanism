#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "cs457_crypto.h"


/*
 * small demo to check function correctness
 */
int main(int argc, char **argv)
{
	//unsigned char *plaintext = (unsigned char *)"Secret message";
	unsigned char plaintext[2048/8] = "Many of Shakespeare's plays were published in editions of varying quality and accuracy in his lifetime."; //size 256, key length : 2048
	unsigned char *key = aes_read_key();
	//unsigned char *ciphertext = malloc(strlen(plaintext) + AES_BS);
	unsigned char ciphertext[4098];
	//unsigned char *decrypted = malloc(strlen(plaintext) + AES_BS);
	unsigned char decrypted[4098];

	int ciphertext_len, plaintext_len = strlen(plaintext);

	RSA *srv_priv_key, *srv_pub_key, *cli_priv_key, *cli_pub_key;

	int encrypted_size, decrypted_size;


	printf("plaintext: %s, %d\n", plaintext, plaintext_len);

	ciphertext_len = aes_encrypt(plaintext, plaintext_len, key, NULL, ciphertext);

	printf("encrypted: %d\n", ciphertext_len);
	print_hex(ciphertext, ciphertext_len);

	/* decrypts the data and returns the plaintext size */
	plaintext_len = aes_decrypt(ciphertext, ciphertext_len, key, NULL, decrypted);
	/* Add a NULL terminator. We are expecting printable text */
  	decrypted[plaintext_len] = '\0';
	printf("decrypted: %s, %d\n", decrypted, plaintext_len);

printf("\n");
	srv_pub_key = rsa_read_key(S_PUB_KF); /* read server public key */

	cli_priv_key = rsa_read_key(C_PRV_KF); /* read client private key */

	srv_priv_key = rsa_read_key(S_PRV_KF); /* read server private key */

	cli_pub_key = rsa_read_key(C_PUB_KF); /* read client public key */

printf("\n");
    //RSA_print_fp(stdout, cli_pub_key, 1);

   	//printf("\nplaintext_len: %d\nciphertext_len: %d\n\n", plaintext_len, strlen(ciphertext));

	encrypted_size = rsa_prv_encrypt(plaintext,plaintext_len,cli_priv_key,ciphertext);
	if(encrypted_size == -1){
	    ERR(1, "Private key encryption failed");
	}
	printf("Encrypted length = %d\n",encrypted_size);
	 
	decrypted_size = rsa_pub_decrypt(ciphertext,encrypted_size,cli_pub_key,decrypted);
	if(decrypted_size == -1){
	   ERR(1, "Public key decryption failed");
	}
	printf("Decrypted Text = %s\n",decrypted);
	printf("Decrypted Length = %d\n",decrypted_size);

	//--------------------------------------------------------------------------------------
printf("\n");

	encrypted_size = rsa_pub_encrypt(plaintext,plaintext_len,cli_pub_key,ciphertext);

	if(encrypted_size == -1){
	    ERR(1, "Public key encryption failed");
	}
	printf("Encrypted length = %d\n",encrypted_size);

	
	decrypted_size = rsa_prv_decrypt(ciphertext,encrypted_size,cli_priv_key,decrypted);

	if(decrypted_size == -1){
	    ERR(1, "Private key decryption failed");
	}
	printf("Decrypted Text = %s\n",decrypted);
	printf("Decrypted Length = %d\n",decrypted_size);

	return 0;
}

/* EOF */
