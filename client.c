#include <sys/socket.h>
#include <netinet/in.h> 
#include <arpa/inet.h> 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "cs457_crypto.h"


/*
 * prints the usage message
 */
void usage(void)
{
	printf(
	    "\n"
	    "Usage:\n"
	    "    client -i IP -p port -m message\n"
	    "    client -h\n"
	);
	printf(
	    "\n"
	    "Options:\n"
	    "  -i  IP         Server's IP address (xxx.xxx.xxx.xxx)\n"
	    "  -p  port       Server's port\n"
	    "  -m  message    Message to server\n"
	    "  -h             This help message\n" 
	);
	exit(EXIT_FAILURE);
}


/*
 * checks the cmd arguments
 */
void check_args(char *ip, unsigned char *msg, int port)
{
	int err;

	err = 0;
	if (!ip) {
		printf("No IP provided\n");
		err = 1;
	}
	if (!msg) {
		printf("No message provided\n");
		err = 1;
	}
	if (port == -1) {
		printf("No port provided\n");
		err = 1;
	}
	if (err)
		usage();
}


/*
 * simple chat client with RSA-based AES 
 * key-exchange for encrypted communication
 */ 
int main(int argc, char *argv[])
{
	int cfd;				/* comm file descriptor	 */
	int port;				/* server port */
	int err;				/* errors */
	int opt;				/* cmd options */

	int plain_len;				/* plaintext size */
	int cipher_len;				/* ciphertext size */

	size_t rxb;				/* received bytes (from server) */
	size_t txb;				/* transmitted bytes */
	char *sip;				/* server IP */

	struct sockaddr_in srv_addr;	/* server socket address */

	unsigned char *msg;			/* message to server */
	unsigned char *aes_key;			/* AES key	*/

	unsigned char plaintext[BUFLEN];	/* plaintext buffer	 */
	unsigned char ciphertext[BUFLEN];	/* ciphertext buffer */

	RSA *c_prv_key;				/* client private key	 */
	RSA *s_pub_key;				/* server public key	 */

	cfd = -1;
	port = -1;
	sip = NULL;
	
	strcpy(plaintext, INIT_MSG); /* init message */

	/* get options */
	while ((opt = getopt(argc, argv, "i:m:p:h")) != -1) {
		switch (opt) {
		case 'i':
			sip = strdup(optarg);
			break;
		case 'm':
			msg = (unsigned char *)strdup(optarg);
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 'h':
		default:
			usage();
		}
	}

	/* check cmd args */
	check_args(sip, msg, port);

	/* Create network socket */
	if ((cfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		perror("Opening TCP sending socket");
		exit(EXIT_FAILURE);
	}

	/* Set parameters for server address */
	memset(&srv_addr, 0, sizeof(srv_addr));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(port);

	//srv_addr.sin_addr.s_addr = inet_addr(sip);
	if(inet_pton(AF_INET, sip, &srv_addr.sin_addr)<=0) { 
        printf("\nInvalid address/ Address not supported \n"); 
        exit(EXIT_FAILURE);
    }
    printf("port: %d, sip: %s\n", ntohs(srv_addr.sin_port), sip);
	/* connect to server */
	if (connect(cfd, (struct sockaddr*)&srv_addr, sizeof(srv_addr)) == -1) {
		perror("Connect to TCP server");
		exit(EXIT_FAILURE);	
	}

	/* load keys */

    c_prv_key = rsa_read_key(C_PRV_KF); /* read client private key */
    s_pub_key = rsa_read_key(S_PUB_KF); /* read server public key */


	/* perform the AES key exchange */

	/* 
	 * encrypt the init message
	 * and send it to the server
	 */

    cipher_len = rsa_pub_priv_encrypt(plaintext,strlen(plaintext),s_pub_key,c_prv_key,ciphertext);

	/* Send message to server */
	txb = send(cfd, ciphertext, cipher_len, 0);
	if(txb == -1){
		ERR(1, "Client TCP send");
	}

	/*
	 * receive the key from the server,
	 * decrypt it and register it
	 */
	rxb = read(cfd, ciphertext, sizeof(ciphertext)); /* read server message */
	if(rxb == -1){
		ERR(1, "Client TCP read");
	}

	aes_key = malloc(AES_BS);
	plain_len = rsa_pub_priv_decrypt(ciphertext,rxb,s_pub_key,c_prv_key,aes_key);

	printf("\n");

	/* encrypt the message with the AES key */

	cipher_len = aes_encrypt(msg, strlen(msg), aes_key, NULL, ciphertext);

	/* send the encrypted message */
	txb = send(cfd, ciphertext, cipher_len, 0);
	if(txb == -1){
		ERR(1, "Client TCP send");
	}

	/* cleanup */

	return 0;
}

/* EOF */
