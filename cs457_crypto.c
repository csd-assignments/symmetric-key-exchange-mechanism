#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/err.h>


/* error reporting helpers */
#define ERRX(ret, str) \
    do { fprintf(stderr, str "\n"); exit(ret); } while (0)
#define ERR(ret, str) \
    do { fprintf(stderr, str ": %s\n", strerror(errno)); exit(ret); } while (0)

/* buffer size */
#define BUFLEN	2048

/* key files*/
#define AES_KF		"keys/aes_key.txt"
#define S_PUB_KF	"keys/srv_pub.pem"
#define S_PRV_KF	"keys/srv_priv.pem"
#define C_PUB_KF	"keys/cli_pub.pem"
#define C_PRV_KF	"keys/cli_priv.pem"

/* AES block size */
#define AES_BS 16


void handleErrors(void)
{
  ERR_print_errors_fp(stderr);
  abort();
}

/* --------------------------- conversion helpers --------------------------- */


/*
 * converts half printable hex value to integer
 */
int half_hex_to_int(unsigned char c)
{
	if (isdigit(c))
		return c - '0';

	if ((tolower(c) >= 'a') && (tolower(c) <= 'f'))
		return tolower(c) + 10 - 'a';

	return 0;
}


/*
 * converts a printable hex array to bytes
 */
char *hex_to_bytes(char *input)
{
	int i;
	char *output;

	output = NULL;
	if (strlen(input) % 2 != 0)
		ERRX(0, "reading hex string");

	output = calloc(strlen(input) / 2, sizeof(unsigned char));
	if (!output)
		ERRX(1, "h2b calloc");

	for (i = 0; i < strlen(input); i+= 2) {
		output[i / 2] = ((unsigned char)half_hex_to_int(input[i])) *
		    16 + ((unsigned char)half_hex_to_int(input[i + 1]));
	}
	
	return output;
}


/*
 * Prints the hex value of the input
 * 16 values per line
 */
void print_hex(unsigned char *data, size_t len)
{
	size_t i;

	if (!data)
		printf("NULL data\n");
	else {
		for (i = 0; i < len; i++) {
			if (!(i % 16) && (i != 0))
				printf("\n");
			printf("%02X ", data[i]);
		}
		printf("\n");
	}
}


/* ----------------------------- key management ----------------------------- */


/*
 * retrieves an AES key from the key file
 */
unsigned char *aes_read_key(void)
{
	unsigned char *key;
	int i = 0, c;
	FILE *file;

	key = malloc(sizeof(char));

	file = fopen(AES_KF, "r");
	if (file) {
    	while ((c = getc(file)) != EOF){ // \n ??
    		key = realloc(key, strlen(key)+1);
    		key[i++] = c;
    	}
    	fclose(file);
	} else {
		ERR(1, "Error: opening file aes_key.txt");
	}
	return key;
}

/* 
 * retrieves an RSA key from the key file
 */
RSA *rsa_read_key(char *kfile)
{
	RSA *rsa = NULL;
    BIO *keybio = NULL;

    rsa = RSA_new();

    /* Use bio library because data are stored in byte form */

    keybio = BIO_new(BIO_s_file());

	if(BIO_read_filename(keybio, kfile)) {

		if( !strcmp(kfile, S_PUB_KF) || !strcmp(kfile, C_PUB_KF) ){ /* public keys */

			rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, NULL, NULL);
			//printf("public key read\n");

		} else if( !strcmp(kfile, S_PRV_KF) || !strcmp(kfile, C_PRV_KF) ){ /* private keys */

			rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
			//printf("private key read\n");

		} else {
			ERR(1, "Wrong file name");
		}

	} else ERR(1, "Cannot open .pem file");

	return rsa;
}


/* ----------------------------- AES functions ------------------------------ */


/*
 * encrypts the data with 128-bit AES ECB
 * iv = initialization vector
 */
int aes_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
    unsigned char *iv, unsigned char *ciphertext)
{
	printf("Encryption\n");

	EVP_CIPHER_CTX *ctx;

  	int len, ciphertext_len;

  	/* Create the encryption context */
  	if(!(ctx = EVP_CIPHER_CTX_new())) {
  		handleErrors();
  	}

  	/* Initialise the encryption operation. 
  	 * 128 bit AES key */
  	if(EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv) != 1){
  		handleErrors();
  	}

  	/* Provide the message to be encrypted and obtain the encrypted output */
  	if(EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len) != 1){
  		handleErrors();
  	}
  	ciphertext_len = len;

  	/* Finalise the encryption */
  	if(EVP_EncryptFinal_ex(ctx, ciphertext + len, &len) != 1) {
  		handleErrors();
  	}
  	ciphertext_len += len;

  	/* Clean up */
  	EVP_CIPHER_CTX_free(ctx);
	return ciphertext_len;
}


/*
 * decrypts the data and returns the plaintext size
 */
int aes_decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
    unsigned char *iv, unsigned char *plaintext)
{
	printf("Decryption\n");
	EVP_CIPHER_CTX *ctx;

  	int len, plaintext_len;

  	/* Create the decryption context */
  	if(!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

  	/* Initialise the decryption operation */
  	if(EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv) != 1) handleErrors();

  	/* Provide the message to be decrypted, and obtain the plaintext output */
  	if(EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len) != 1) handleErrors();
  	
  	plaintext_len = len;

  	/* Finalise the decryption. Further plaintext bytes may be written at this stage.*/
  	if(EVP_DecryptFinal_ex(ctx, plaintext + len, &len) != 1) handleErrors();
  	
  	plaintext_len += len;

  	/* Clean up */
  	EVP_CIPHER_CTX_free(ctx);
  	//printf("plaintext: %s\n", plaintext);
  	return plaintext_len;
}


/* ----------------------------- RSA functions ------------------------------ */


/*
 * RSA public key encryption
 */
int rsa_pub_encrypt(unsigned char *plaintext, int plaintext_len,
    RSA *key, unsigned char *ciphertext)
{
	int encrypted_size;

    encrypted_size = RSA_public_encrypt(plaintext_len,plaintext,ciphertext,key,RSA_PKCS1_PADDING);
    return encrypted_size;
}


/*
 * RSA private key decryption
 */
int rsa_prv_decrypt(unsigned char *ciphertext, int ciphertext_len,
    RSA *key, unsigned char *plaintext)
{
	int decrypted_size;
	// plaintext = decrypted message
	decrypted_size = RSA_private_decrypt(ciphertext_len,ciphertext,plaintext,key,RSA_PKCS1_PADDING);
    return decrypted_size;
}


/*
 * RSA private key encryption
 */
int rsa_prv_encrypt(unsigned char *plaintext, int plaintext_len,
    RSA *key, unsigned char *ciphertext)
{
	int encrypted_size;

	encrypted_size = RSA_private_encrypt(plaintext_len,plaintext,ciphertext,key,RSA_PKCS1_PADDING);
    return encrypted_size;
}


/*
 * RSA public key decryption
 */
int rsa_pub_decrypt(unsigned char *ciphertext, int ciphertext_len,
    RSA *key, unsigned char *plaintext)
{
	int decrypted_size;

	decrypted_size = RSA_public_decrypt(ciphertext_len,ciphertext,plaintext,key,RSA_PKCS1_PADDING);
    return decrypted_size;
}


/*
 * RSA Public(Private) encryption
 */
int rsa_pub_priv_encrypt(unsigned char *plaintext, int plaintext_len,
    RSA *pub_k, RSA *priv_k, unsigned char *ciphertext)
{
	int cipher_len, i, tmp;
	char encrypted[BUFLEN];
	/* Encrypt using client's private */
    cipher_len = rsa_prv_encrypt(plaintext,plaintext_len,priv_k,encrypted);
	if(cipher_len == -1){
	    ERR(1, "Private key encryption failed");
	}
	//printf("Encrypted length = %d, %d\n",cipher_len, RSA_size(pub_k));

	/* Encrypt using server's public */
	cipher_len = rsa_pub_encrypt(encrypted,RSA_size(pub_k)-11,pub_k,ciphertext); /* 11 bytes padding */
	if(cipher_len == -1){
	    ERR(1, "Public key encryption failed");
	}

	tmp = cipher_len;

	i = RSA_size(pub_k)-11;
	//printf("i: %d\n", i);
	cipher_len = rsa_pub_encrypt(encrypted + i,11,pub_k,ciphertext + RSA_size(pub_k)); /* 256-11 bytes padding */
	if(cipher_len == -1){
	    ERR(1, "Public key encryption failed");
	}
	//printf("Encrypted length = %d\n",cipher_len);

	return tmp + cipher_len;
}


/*
 * RSA Public(Private) decryption
 */
int rsa_pub_priv_decrypt(unsigned char *ciphertext, int ciphertext_len,
    RSA *pub_k, RSA *priv_k, unsigned char *plaintext)
{
	int plain_len, decrypted_len, i, tmp;
	char decrypted[BUFLEN];

    /* Decrypt using server's private key */
    plain_len = rsa_prv_decrypt(ciphertext,RSA_size(priv_k),priv_k,decrypted);
	if(plain_len == -1){
	    ERR(1, "Private key decryption failed");
	}

	//printf("plain_len: %d\n", plain_len);
	i = RSA_size(priv_k);
	tmp = rsa_prv_decrypt(ciphertext + i,RSA_size(priv_k),priv_k,decrypted + plain_len);
	if(tmp == -1){
	    ERR(1, "Private key decryption failed");
	}
	//printf("tmp: %d\n", tmp);
	decrypted_len = tmp + plain_len; /* to size tou private decryption */
	//printf("Decrypted Length = %d\n",decrypted_len);


	/* Decrypt using client's public key */
	plain_len = rsa_pub_decrypt(decrypted,RSA_size(pub_k),pub_k,plaintext);
	if(plain_len == -1){
	   ERR(1, "Public key decryption failed");
	}
	//i = RSA_size(pub_k);

	//printf("Decrypted Text = %s\n",plaintext);

	return plain_len;
}

/* EOF */
