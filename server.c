#include <sys/socket.h>
#include <netinet/in.h> 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "cs457_crypto.h"

/* 
 * Default server port 
 *
 * Be careful when using this port on 
 * CSD's machines. Read the README file 
 * and select an other port by changing 
 * this value or by using -p <port> 
 */
#define DEFAULT_PORT	3747

/*
 * prints the usage message
 */
void usage(void)
{
	printf(
	    "\n"
	    "Usage:\n"
	    "    server [-p port]\n"
	    "    server -h\n"
	);
	printf(
	    "\n"
	    "Options:\n"
	    "  -p  port       Server's port\n"
	    "  -h             This help message\n" 
	);
	exit(EXIT_FAILURE);
}

/*
 * simple chat server with RSA-based AES 
 * key-exchange for encrypted communication
 */ 
int main(int argc, char *argv[])
{
	int lfd;				/* listen file descriptor */
	int cfd;				/* comm file descriptor   */
	int port;				/* server port		  */
	int err;				/* errors		  */
	int opt;				/* cmd options		  */
	int optval;				/* socket options	  */
	
	int plain_len;				/* plaintext size	  */
	int cipher_len;				/* ciphertext size	  */
	int decrypted_len;

	size_t rxb;				/* received bytes (from client) */
	size_t txb;				/* transmitted bytes */

	struct sockaddr_in srv_addr;		/* server socket address  */

	unsigned char *aes_key;			/* AES key	*/
	unsigned char plaintext[BUFLEN];	/* plaintext buffer	  */
	unsigned char ciphertext[BUFLEN];	/* ciphertext buffer  */
	unsigned char decrypted[BUFLEN];	/* final decrypted */

	RSA *s_prv_key;				/* server private key	  */
	RSA *c_pub_key;				/* client public key	  */

	socklen_t addrlen; /* on return it will contain the actual size of the peer address */

	addrlen = sizeof(srv_addr);

	lfd = -1;
	cfd = -1;
	optval = 1;
	port = DEFAULT_PORT;

	/* get options */
	while ((opt = getopt(argc, argv, "p:h")) != -1) {
		switch (opt) {
		case 'p':
			port = atoi(optarg);
			break;
		case 'h':
		default:
			usage();
		}
	}

	/* Create network socket */
	if ((lfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		perror("Opening TCP listening socket");
		exit(EXIT_FAILURE);
	}

	memset(&srv_addr, 0, sizeof(srv_addr));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(port);
	srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	printf("port: %d\n", port);

	/*
	 * this will save them from:
	 * "ERROR on binding: Address already in use"
	 */

	/* Bind socket to port */
	if (bind(lfd, (struct sockaddr*)&srv_addr, sizeof(srv_addr)) == -1) {
		perror("TCP bind");
		exit(EXIT_FAILURE);
	}
	/* 
	 * bind and listen the socket
	 * for new client connections
	 */
	if (listen(lfd, 0) == -1) {
		perror("TCP listen");
		exit(EXIT_FAILURE);
	}

	/* load keys */

	s_prv_key = rsa_read_key(S_PRV_KF); /* read server private key */
	c_pub_key = rsa_read_key(C_PUB_KF); /* read client public key */

	aes_key = aes_read_key();
	//printf("aes_key: %s\n", aes_key);

	/* accept a new client connection */
	printf("Waiting for connection...\n");
	if ((cfd = accept(lfd, (struct sockaddr*)&srv_addr, &addrlen)) == -1) {
		perror("TCP accept");
		exit(EXIT_FAILURE);
	}
	//printf("Connection accepted\n");

	/* Receive the init message encrypted */
	rxb = read(cfd, ciphertext, sizeof(ciphertext)); /* the number of bytes read is returned */
	if(rxb == -1){
		ERR(1, "Server TCP read");
	}

	//printf("init message length = %d\n", rxb);

	plain_len = rsa_pub_priv_decrypt(ciphertext, rxb, c_pub_key, s_prv_key, plaintext);

	/* send the AES key */
	if(!strcmp(plaintext, INIT_MSG)){
		cipher_len = rsa_pub_priv_encrypt(aes_key, strlen(aes_key),c_pub_key, s_prv_key, ciphertext);

		txb = send(cfd, ciphertext, cipher_len, 0);
		if(txb == -1){
			ERR(1, "Server TCP send");
		}

		/* receive the encrypted message */
		rxb = read(cfd, ciphertext, sizeof(ciphertext)); /* read server message */
		if(rxb == -1){
			ERR(1, "Server TCP read");
		}
		//printf("rxb: %d\n", rxb);
		/* Decrypt the message and print it */
		plain_len = aes_decrypt(ciphertext, rxb, aes_key, NULL, decrypted);
		/* Add a NULL terminator. We are expecting printable text */
	  	decrypted[plain_len] = '\0';
		printf("Decrypted Text: %s, %d bytes\n", decrypted, plain_len);

	/* cleanup */

	} else {
		printf("Init message was not the one expected\n");
	}
	return 0;
}

/* EOF */
